//
//  GameScene.swift
//  Fishing
//
//  Created by Prabhjinder Singh on 2019-10-22.
//  Copyright © 2019 Prabhjinder. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
   
        
        
        var hook: SKSpriteNode!
        var mouseX:CGFloat = 0
        var mouseY:CGFloat = 0
      //  var countryList : Array<Country> = Array()
        var fish1Array:[SKSpriteNode] = [SKSpriteNode]()
        var numLoops = 0
        var hookedFishArray:[SKSpriteNode] = [SKSpriteNode]()
        var fishMovement:[String] = [String]()
        

        
        override func didMove(to view: SKView) {
        // Add your hook
            
    //        let background = SKSpriteNode(imageNamed:"fishingBackground")
    //        background.position = CGPoint(x:size.width/2, y:size.height/2)
    //        addChild(background)
            
            
            self.hook = SKSpriteNode(imageNamed: "hook")
            self.hook.position = CGPoint(x: 500, y: 900)
            addChild(hook)
            
        }



        override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {

        }
        
        func moveToPosition(oldPosition: CGPoint, newPosition: CGPoint) {

            let xDistance = abs(oldPosition.x - newPosition.x)
            let yDistance = abs(oldPosition.y - newPosition.y)
            let distance = sqrt(xDistance * xDistance + yDistance * yDistance)
            let width = self.view?.frame.size.width
            let height = self.view?.frame.size.height
            let sceneDiagonal = sqrt(width! * width! + height! * height!)

            self.run(SKAction.move(to: newPosition, duration: Double(distance / sceneDiagonal / 2)))
        }
        
        override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {

            let touchLocation = touches.first!.location(in: self)
            if(self.hook.contains(touchLocation)){
                //moveToPosition(oldPosition: self.hook.position, newPosition: touchLocation)
                moveHook(mouseXPosition: touchLocation.x, mouseYPostion: touchLocation.y)
        }


        }
              
          func fish1() {
              // Add a cat to a static location
              let fish1 = SKSpriteNode(imageNamed: "fish1")
              
              // generate a random x position
              
              let randomXPos = CGFloat.random(in: 100 ... size.width-150)
            let randomYPos = CGFloat.random(in: 0 ... hook.position.y-50)
              fish1.position = CGPoint(x:randomXPos, y:randomYPos)
              
              // add the cat to the screen
              addChild(fish1)
              
              // add the cat to the array
              self.fish1Array.append(fish1)
            self.fishMovement.append("right")
              
          }
        func moveHook(mouseXPosition:CGFloat, mouseYPostion:CGFloat) {
            
            // move the zombie towards the mouse
            // @TODO: Get the android code for moving bullet towards enemey
            // Implement the algorithm in Swift
            
            // 1. calculate disatnce between mouse and zombie
            let a = (mouseXPosition - self.hook.position.x);
            let b = (mouseYPostion - self.hook.position.y);
            let distance = sqrt((a * a) + (b * b))
            
            // 2. calculate the "rate" to mov
            let xn = (a / distance)
            let yn = (b / distance)
            
            // 3. move the bullet
            self.hook.position.x = self.hook.position.x + (xn * 25);
            self.hook.position.y = self.hook.position.y + (yn * 25);
            
        }
        
            
        override func update(_ currentTime: TimeInterval) {
            // Called before each frame is rendered
            
       // move the zomibe
            
            numLoops = numLoops + 1
                  if (numLoops % 50 == 0 && numLoops <= 499)
                  {
                      // make a cat
                      self.fish1()
                    

                  }
                  else if(numLoops == 475){
                    print("hello")
                        for (index, fish1) in self.fish1Array.enumerated()
                        {
                            fish1.removeFromParent();
                            //fish1Array.remove(at: index);
                        }
                    //fish1Array.removeAll()
                    print(hookedFishArray);
                    
            }
                
            
            
         for (index, fish1) in self.fish1Array.enumerated() {
                      if (self.hook.frame.intersects(fish1.frame) == true)
                      {
                                hookedFishArray.append(fish1)
                                //print(hookedFishArray)
                                //fishMovement.remove(at: index)
                                fish1.removeFromParent()
                                
                                addChild(fish1);
                        
                      }
            else
                      {
                      
                        if(fish1.position.x > size.width-150 ){
                            fishMovement[index] = "left"
                            let facingLeft = SKAction.scaleX(to: -1, duration: 0)
                            fish1.run(facingLeft)
                            
                        }
                        else if(fish1.position.x < 100)
                        {
                            fishMovement[index] = "right"
                             let facingRight = SKAction.scaleX(to: 1, duration: 0)
                            fish1.run(facingRight)
                        }

                        if(fishMovement[index] == "left")
                        {
                            fish1.position.x -= 10
                        }
                        else
                        {
                            fish1.position.x += 10

                        }
            
            }
                  }
            for (index, hookedFish) in self.hookedFishArray.enumerated(){
                hookedFish.position.x = hook.position.x
                hookedFish.position.y = hook.position.y - 55
            }
            
             //self.moveHook(mouseXPosition: self.mouseX, mouseYPostion: self.mouseY)
        }
    }
